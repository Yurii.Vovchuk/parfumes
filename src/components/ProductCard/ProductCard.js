import React from 'react';
import styles from './ProductCard.module.scss';
import Button from '../Button/Button';
import ProductItem from '../ProductItem/ProductItem';
import Description  from '../ProductItem/Description';


const ProductCard = ( { title, url } ) => {
  
  return (
    <div className={styles.containerCard}>
      <div className={styles.containerProduct}>
        <ProductItem 
          src={url}
          alt='product 1'
        />
      </div>
      <div className={styles.container}>
        <Button title='new'/>
        <div className={styles.iconBg}>
          <img src='images/icon-libra.png' alt='icon' />
        </div>
      </div>
      <Description 
        
        productTitle={title}
        description='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.'
      />
    </div>
  )
}

export default ProductCard
