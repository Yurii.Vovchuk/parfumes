import React from 'react';
import styles from './Description.module.scss';

const Description = ({ productTitle, description }) => {
  return (
    <div className={styles.description}>
      <h2 className={styles.descriptionTitle}>{productTitle}</h2>
      <p className={styles.descriptionText}> {description} </p>
    </div>
  )
}

export default Description
