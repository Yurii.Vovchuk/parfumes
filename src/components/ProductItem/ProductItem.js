
import React from 'react';
import styles from './ProductItem.module.scss';

const ProductItem = ({ src, alt }) => {

  return (
    <div className={styles.container}>
      <img 
        className={styles.productImg}
        src={src} 
        alt={alt} 
        />
      
    </div>
  )
}

export default ProductItem
