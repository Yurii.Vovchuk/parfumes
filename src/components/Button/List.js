import React from 'react';
import styles from './Button.module.scss';

const Button = ({ title }) => {

  return (
    <button className={styles.button} title={title}>
      {title}
    </button>
  )
}
export default Button;