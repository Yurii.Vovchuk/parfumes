import "./App.css";
import ProductCard from "./components/ProductCard/ProductCard";

function App() {
  const products = [
    {
      title: "Шампунь",
      url: "/images/image1.png",
    },
    {
      title: "Шампунь",
      url: "/images/image1.png",
    },
  ];

  return (
    <div className="App">
      {products.map((item, index) => (
        <ProductCard title={item.title} url={item.url} />
      ))}
    </div>
  );
}

export default App;
